# Maintainer: Natanael Copa <ncopa@alpinelinux.org>
pkgname=conky
pkgver=1.20.2
pkgrel=0
pkgdesc="Advanced, highly configurable system monitor for X based on torsmo"
url="https://github.com/brndnmtthws/conky"
arch="all"
license="GPL-3.0-or-later"
makedepends="
	alsa-lib-dev
	cairo-dev
	cmake
	curl-dev
	gawk
	glib-dev
	imlib2-dev
	libxdamage-dev
	libxext-dev
	libxft-dev
	libxinerama-dev
	libxml2-dev
	linux-headers
	lua5.4-dev
	ncurses-dev
	pango-dev
	samurai
	tolua++
	wayland-dev
	libxi-dev
	wayland-protocols
	wireless-tools-dev
	"
subpackages="$pkgname-doc"
source="$pkgname-$pkgver.tar.gz::https://github.com/brndnmtthws/conky/archive/v$pkgver.tar.gz"
options="!check"

build() {
	cmake -B build -G Ninja \
		-DRELEASE=ON \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DBUILD_CURL=ON \
		-DBUILD_XDBE=ON \
		-DBUILD_IMLIB2=ON \
		-DBUILD_RSS=ON \
		-DBUILD_WLAN=ON \
		-DBUILD_I18N=OFF \
		-DBUILD_LUA_CAIRO=ON \
		-DBUILD_WAYLAND=ON \
		-DLUA_LIBRARIES="/usr/lib/lua5.4/liblua.so"
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
	install -D -m644 COPYING $pkgdir/usr/share/licenses/$pkgname/LICENSE
}

sha512sums="
b516db38af5c20b101520356cd4f50d147869e7ed2bc84287e8a464e32bb306655edbac074da278d4732ee87f02c52d99cb4c117556e0d08f5be911a2a789618  conky-1.20.2.tar.gz
"
